package cz.vse.dumpstermapp

import android.app.Application
import cz.vse.dumpstermapp.repository.di.Module
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        // start Koin
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(Module.appModule)
        }
//
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return
//        }
//        LeakCanary.install(this)
    }
}
