package cz.vse.dumpstermapp.main

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Point
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.util.AttributeSet
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import com.google.firebase.Timestamp
import cz.vse.dumpstermapp.R
import cz.vse.dumpstermapp.main.home.fragment.DumpsterAddFragment
import cz.vse.dumpstermapp.main.home.model.HomeViewModel
import cz.vse.dumpstermapp.repository.entity.Comment
import cz.vse.dumpstermapp.repository.entity.Dumpster
import cz.vse.dumpstermapp.repository.shared.Shared
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_tutorial.*
import org.koin.core.KoinComponent


class MainActivity : AppCompatActivity(), KoinComponent, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener {

    companion object {
        val TAG: String = MainActivity::class.java.simpleName
    }

    private lateinit var mViewModel: HomeViewModel

    private lateinit var mMap: GoogleMap

    private var mCameraPosition: CameraPosition? = null

    // The entry point to the Places API.
    private var mPlacesClient: PlacesClient? = null

    // The entry point to the Fused Location Provider.
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null

    // A default location (Praha, Czech Republic) and default zoom to use when location permission is
    // not granted.
    private val mDefaultLocation = LatLng(50.08804, 14.42076)
    private val DEFAULT_ZOOM = 15
    private val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
    private var mLocationPermissionGranted = false

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    var mLastKnownLocation: Location? = null

    var previousPosition: LatLng? = null

    // Keys for storing activity state.
    private val KEY_CAMERA_POSITION = "camera_position"
    private val KEY_LOCATION = "location"

    // Used for selecting the current place.
    private val M_MAX_ENTRIES = 5
    private var mLikelyPlaceNames: Array<String>? = null
    private var mLikelyPlaceAddresses: Array<String>? = null
    private var mLikelyPlaceAttributions: Array<List<*>>? = null
    private var mLikelyPlaceLatLngs: Array<LatLng>? = null

    // location of added marker
    var markers: MutableCollection<Marker> = ArrayList()
    var focusedMarker: Marker? = null

    private var dumpsters: MutableList<Dumpster> = ArrayList()
    var commentedDumpster: Dumpster? = null

    var previousBottomSheetHeight:Int? = null

    var actionMenu: Menu? = null

    lateinit var markerIcon: BitmapDescriptor
    lateinit var focusedMarkerIcon:BitmapDescriptor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getLocationPermission()

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        mViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION)
        }

        Places.initialize(applicationContext, getString(R.string.google_maps_key))
        mPlacesClient = Places.createClient(this)
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        markerIcon = BitmapDescriptorFactory.fromAsset("marker.png")
        focusedMarkerIcon = BitmapDescriptorFactory.fromAsset("focused_marker.png")

        mViewModel.createObservable().observe(this, Observer {
            if (it!=null && findNavController(R.id.nav_host_fragment_sheet).currentDestination?.id == R.id.dumpsterAddFragment){

                // todo (findViewById<View>(R.id.dumpsterAddFragment) as DumpsterAddFragment).Toast("Dumpster successfully created")

                commentedDumpster = it
                Log.d("xxx observable",commentedDumpster.toString())
                focusedMarker?.remove()
                hideKeyboard()
                findNavController(R.id.nav_host_fragment_sheet).navigate(R.id.action_dumpsterAddFragment_to_dumpsterCommentFragment)
                centerPosition()
            }else{

                (findViewById<View>(R.id.dumpsterAddFragment) as DumpsterAddFragment).toast("Error communicating with database.")

                Log.d("xxx","dumpster not created" + commentedDumpster.toString())
            }
        })

        mViewModel.getObservable().observe(this, Observer {
            if(::mMap.isInitialized){
                for (Marker in markers){
                    //todo nefunguje, vyresit
                    Marker.remove()
                }
                markers.clear()
                dumpsters.clear()
                dumpsters.addAll(it)
                for (d in it){
                    val markerPosition = LatLng(d.location.getValue("latitude"),d.location.getValue("longitude"))
                    //improvement
                    markers.add(mMap.addMarker(MarkerOptions().title(d.id).draggable(false).position(markerPosition).icon(markerIcon)))
                    //markers.add(mMap.addMarker(MarkerOptions().title(d.id).draggable(false).position(markerPosition).icon(BitmapDescriptorFactory.defaultMarker(125F))))
                }
                if(findNavController(R.id.nav_host_fragment).currentDestination?.id != R.id.navigation_about) {
                    if (findNavController(R.id.nav_host_fragment_sheet).currentDestination?.id != R.id.dumpsterAddFragment) {
                        focusedMarker = markers.find { it.title == focusedMarker?.title }
                        if (focusedMarker != null) {
                            focusedMarker!!.setIcon(focusedMarkerIcon)
                        }
                    }
                    if (findNavController(R.id.nav_host_fragment_sheet).currentDestination?.id == R.id.dumpsterCommentFragment && commentedDumpster != null) {
                        focusedMarker = markers.find { it.title == commentedDumpster?.id }
                        if (focusedMarker != null) {
                            focusedMarker!!.setIcon(focusedMarkerIcon)
                        }
                    }
                }
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.cameraPosition)
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation)
            super.onSaveInstanceState(outState)
        }
    }

    override fun onMapReady(map: GoogleMap) {
        mMap = map
        mMap.setOnMarkerDragListener(this)
        mMap.setOnMarkerClickListener(this)
        mMap.setOnMapClickListener {
            collapseBottomSheet()
        }

        mMap.setOnCameraIdleListener {
            //get dumpsters from database...
            mViewModel.getDumpsters(mMap.projection.visibleRegion.latLngBounds)
        }

        // Prompt the user for permission.
        //getLocationPermission()

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI()

        // Get the current location of the device and set the position of the map.
        getDeviceLocation()
    }

    private fun updateLocationUI() {
        if (mMap == null)
        {
            return
        }
        try
        {
            if (mLocationPermissionGranted)
            {
                mMap.setMyLocationEnabled(true)
                mMap.getUiSettings().setMyLocationButtonEnabled(true)
                getDeviceLocation()
            }
            else
            {
                mMap.setMyLocationEnabled(false)
                mMap.getUiSettings().setMyLocationButtonEnabled(false)
                mLastKnownLocation = null
//                getLocationPermission()
            }
        }
        catch (e:SecurityException) {
            Log.e("Exception: %s", e.message!!)
        }
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private fun getLocationPermission() { /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.applicationContext,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                    actionMenu?.findItem(R.id.action_permission)?.setVisible(false)
                }
            }
        }
        updateLocationUI()
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location
         * of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                val locationResult = mFusedLocationProviderClient!!.lastLocation
                locationResult.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) { // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = task.result
                        if (mLastKnownLocation != null) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    LatLng(mLastKnownLocation!!.latitude,
                                            mLastKnownLocation!!.longitude), DEFAULT_ZOOM.toFloat()))
                        }
                    } else {
                        //Log.d(TAG, "Current location is null. Using defaults.")
                        //Log.e(TAG, "Exception: %s", task.exception!!)
                        mMap.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM.toFloat()))
                        mMap.uiSettings.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message.toString())
        }

    }

    override fun onResume() {
        // aby se po zavreni bottom sheetu obnovil do start destination
        val bottomSheet = findViewById<View>(R.id.bottomSheetDialogFragment)
        if (bottomSheet!=null){
            val behavior = BottomSheetBehavior.from(bottomSheet)
            behavior.addBottomSheetCallback(object: BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
                    if (newState == STATE_COLLAPSED){

                        if (findNavController(R.id.nav_host_fragment_sheet).currentDestination?.id== R.id.dumpsterAddFragment){
                            focusedMarker!!.remove()
                            focusedMarker = null
                            centerOnCollapsed()
                        }else/* if (findNavController(R.id.nav_host_fragment_sheet).currentDestination?.id== R.id.dumpsterListFragment)*/{
                            if (focusedMarker != null){
                                focusedMarker = markers.find { it.title == focusedMarker?.title }
                                focusedMarker!!.setIcon(markerIcon)
                            }
                            focusedMarker = null
                        }

                        Shared.dumpsterTitle?.unsetTitle()
                        findNavController(R.id.nav_host_fragment_sheet).navigate(R.id.dumpsterListFragment)
                    }
                    if (newState == STATE_EXPANDED){
                        bottomSheet.requestLayout()
                        bottomSheet.invalidate()
                        //centerPosition()
                    }
                    //todo analyze centerPosition()
                }
                override fun onSlide(bottomSheet: View, slideOffset: Float) {

                }
            })
        }
        super.onResume()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        actionMenu = menu
        if (mLocationPermissionGranted){
            actionMenu?.findItem(R.id.action_permission)?.setVisible(false)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_about -> {
                if(findNavController(R.id.nav_host_fragment).currentDestination?.id != R.id.navigation_about){
                    findNavController(this, R.id.nav_host_fragment).navigate(R.id.navigation_about)
                }
                true
            }
            R.id.action_tutorial ->{
                if(findNavController(R.id.nav_host_fragment).currentDestination?.id == R.id.navigation_about){
                    //findNavController(this, R.id.nav_host_fragment).navigate(R.id.navigation_home)
                    findNavController(R.id.nav_host_fragment).popBackStack()
                    // todo very ugly hotfix solution, needs rework
                    Handler().postDelayed({
                        collapseBottomSheet()
                        findViewById<View>(R.id.tutorial).visibility = View.VISIBLE
                        findViewById<View>(R.id.map_shadow).visibility = View.VISIBLE
                    }, 100)
                }else{
                    collapseBottomSheet()
                    findViewById<View>(R.id.tutorial).visibility = View.VISIBLE
                    findViewById<View>(R.id.map_shadow).visibility = View.VISIBLE
                }
                true
            }
            R.id.action_permission -> {
                getLocationPermission()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        if (findNavController(R.id.nav_host_fragment_sheet).currentDestination?.id== R.id.dumpsterAddFragment){
            focusedMarker!!.remove()
            focusedMarker = null
            centerOnCollapsed()
        }
        if(focusedMarker!=null){
            focusedMarker = markers.find { it.title == focusedMarker?.title }
            focusedMarker!!.setIcon(markerIcon)
        }
        focusedMarker = p0
        mViewModel.selectedDumpster.value = dumpsters.find { it.id == p0?.title }
        showBottomSheet()
        centerPosition()
        findNavController(R.id.nav_host_fragment_sheet).navigate(R.id.dumpsterDetailFragment)
        focusedMarker!!.setIcon(focusedMarkerIcon)
        // todo maybe focusedMarker!!.zIndex = Float.MAX_VALUE
        return true
    }

    override fun onMarkerDragEnd(p0: Marker) {
       //newMarker!!.position = p0.position

    }

    override fun onMarkerDragStart(p0: Marker?) {

    }

    override fun onMarkerDrag(p0: Marker?) {

    }

    private fun centerOnCollapsed() {
        val position = mMap.cameraPosition.target
        val point: Point = mMap.projection.toScreenLocation(position)
        val targetPoint = Point(point.x,point.y-previousBottomSheetHeight!!/2)
        val targetPosition = mMap.projection.fromScreenLocation(targetPoint)
        mMap.animateCamera(CameraUpdateFactory.newLatLng(targetPosition), 1000, null)
    }

    private fun centerPosition(){
        val position: LatLng
        if(focusedMarker==null){
            position = mMap.cameraPosition.target
            //position = LatLng(mLastKnownLocation!!.latitude,mLastKnownLocation!!.longitude)
        }else{
            position = focusedMarker!!.position
        }
        previousPosition = position
        val point: Point = mMap.projection.toScreenLocation(position)
        val targetPoint = Point(point.x,point.y+getBottomSheetHeight()/2)
        val targetPosition = mMap.projection.fromScreenLocation(targetPoint)
        mMap.animateCamera(CameraUpdateFactory.newLatLng(targetPosition), 1000, null)
    }

    fun centerDetail(dumpster:Dumpster){
        markers.find { marker->
            if (marker.title == dumpster.id){
                Log.d("xxx","sucess")
                focusedMarker = marker
                centerPosition()
                true
            }else{
                false
            }
        }
    }

    private fun getBottomSheetHeight(): Int {
        val bs = findViewById<View>(R.id.bottomSheetDialogFragment)
        val behavior = BottomSheetBehavior.from(bs)
        return if (behavior.state == STATE_COLLAPSED){
            0
        }else{
            previousBottomSheetHeight = findViewById<View>(R.id.nav_host_fragment_sheet).height
            findViewById<View>(R.id.nav_host_fragment_sheet).height
        }
    }

    private fun collapseBottomSheet(){
        val bottomSheet = findViewById<View>(R.id.bottomSheetDialogFragment)
        val behavior = BottomSheetBehavior.from(bottomSheet)
        hideKeyboard()
        behavior.state = STATE_COLLAPSED
    }

    private fun showBottomSheet() {
        val bottomSheet = findViewById<View>(R.id.bottomSheetDialogFragment)
        val behavior = BottomSheetBehavior.from(bottomSheet)
        behavior.state = STATE_EXPANDED
    }

    fun headerClicked(view: View) {
        // todo zabránit collapsu při zakládání dumpsteru? nebo psaní commentu
        // mozna popup dialog jestli chcete zrusit ?
        val bottomSheet = findViewById<View>(R.id.bottomSheetDialogFragment)
        val behavior = BottomSheetBehavior.from(bottomSheet)
        if (behavior.state == STATE_COLLAPSED){
            showBottomSheet()
        } else {
            collapseBottomSheet()
        }
    }

    fun fabClicked(view: View){
        focusedMarker = mMap.addMarker(MarkerOptions().draggable(true).position(mMap.cameraPosition.target).icon(focusedMarkerIcon).zIndex(Float.MAX_VALUE))
        findNavController(R.id.nav_host_fragment_sheet).navigate(R.id.dumpsterAddFragment)
        showBottomSheet()
        centerPosition()
    }

    override fun onBackPressed() {
        Shared.dumpsterFragment?.onBackPressed() ?: run {
            val bottomSheet = findViewById<View>(R.id.bottomSheetDialogFragment)
            if(bottomSheet!=null){
                val behavior = BottomSheetBehavior.from(bottomSheet)
                //ugly navigation back from comment fragment
                if( findNavController(R.id.nav_host_fragment_sheet).currentDestination?.id == R.id.dumpsterCommentFragment){
                    findNavController(R.id.nav_host_fragment_sheet).popBackStack()
                    if(findNavController(R.id.nav_host_fragment_sheet).currentDestination?.id == R.id.dumpsterAddFragment){
                        collapseBottomSheet()
                    }
                }else{
                    if (behavior.state == STATE_EXPANDED){
                        collapseBottomSheet()
                    } else {
                        super.onBackPressed()
                    }
                }
            }else{this
                super.onBackPressed()
            }

        }
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        view?.let { v ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

    fun createDumpster(text: String) {
        val dumpster = Dumpster(text,true, hashMapOf("latitude" to focusedMarker!!.position.latitude, "longitude" to focusedMarker!!.position.longitude))
        mViewModel.addDumpster(dumpster)
        //todo aby marker zcervenal
    }

    fun createComment(c: String, text: String) {
        Log.d("xxx comment",commentedDumpster.toString())
        val comment = Comment(text,c, Timestamp.now())
        mViewModel.addComment(commentedDumpster!!,comment)
        hideKeyboard()
        mViewModel.selectedDumpster.value = commentedDumpster
        findNavController(R.id.nav_host_fragment_sheet).navigate(R.id.action_dumpsterCommentFragment_to_dumpsterDetailFragment)
        centerPosition()
    }

}