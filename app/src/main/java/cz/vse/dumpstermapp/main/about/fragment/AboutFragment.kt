package cz.vse.dumpstermapp.main.about.fragment

import android.content.Context
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cz.vse.dumpstermapp.R
import cz.vse.dumpstermapp.main.common.IBackAction
import cz.vse.dumpstermapp.repository.shared.Shared
import kotlinx.android.synthetic.main.fragment_about.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.io.IOException
import java.io.InputStream


/**
 * A simple [Fragment] subclass.
 */
class AboutFragment : Fragment(), IBackAction {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        about_web.loadUrl("file:///android_asset/about.html")

        about_close.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun onResume() {
        super.onResume()
        Shared.dumpsterFragment = this
    }

    override fun onPause() {
        super.onPause()
        Shared.dumpsterFragment = null
    }

    override fun onBackPressed() {
        findNavController().popBackStack()
    }

}
