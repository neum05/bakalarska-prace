package cz.vse.dumpstermapp.main.common

interface IBackAction {
    fun onBackPressed()
}