package cz.vse.dumpstermapp.main.home.adapter

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.location.Location
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.GeoPoint
import cz.vse.dumpstermapp.R
import cz.vse.dumpstermapp.main.MainActivity
import cz.vse.dumpstermapp.main.home.fragment.DumpsterListFragment
import cz.vse.dumpstermapp.main.home.model.HomeViewModel
import cz.vse.dumpstermapp.main.home.viewholder.DumpsterViewHolder
import cz.vse.dumpstermapp.repository.entity.Dumpster
import org.koin.core.KoinComponent
import org.koin.core.inject
import kotlin.math.round
import kotlin.math.roundToInt

class DumpsterAdapter(var dumpsters : List<Dumpster>, private var favouritePreferences: SharedPreferences, private val fragment: DumpsterListFragment) : RecyclerView.Adapter<DumpsterViewHolder>(), KoinComponent{

    private val context : Context by inject()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DumpsterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_dumpster, parent,false)
        return DumpsterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dumpsters.size
    }

    override fun onBindViewHolder(holder: DumpsterViewHolder, position: Int) {

        var favouritesSet = favouritePreferences.getStringSet(FAV_KEY,null) ?: mutableSetOf()

        holder.title.text = dumpsters[position].name
        holder.distance.text = getDistanceString(fragment.requireActivity() as MainActivity, dumpsters[position].location.getValue("latitude"),dumpsters[position].location.getValue("longitude"))


        holder.rating.text = getRating(dumpsters[position])

        if (favouritesSet.contains(dumpsters[position].id)){
            holder.favouriteButton.setImageResource(R.drawable.ic_heart_icon_red)
        } else {
            holder.favouriteButton.setImageResource(R.drawable.ic_heart_icon_hollow)
        }

        holder.circle.background.setTint(colorCircle(holder.rating.text.toString()))

        holder.itemLayout.setOnClickListener {
            //Toast.makeText(context, "Clicked ${holder.title.text}", Toast.LENGTH_SHORT).show()
            fragment.showDetailOf(dumpsters[position])
            //todo premistit se na mape a otevrit detail
        }

        holder.favouriteButton.setOnClickListener{view ->

            favouritesSet = favouritePreferences.getStringSet(FAV_KEY,null) ?: mutableSetOf()
            val editor = favouritePreferences.edit()
            val newSet = HashSet<String>()
            newSet.addAll(favouritesSet)

            if (favouritesSet.contains(dumpsters[position].id)){
                newSet.remove(dumpsters[position].id)
                editor.clear()
                editor.putStringSet(FAV_KEY, newSet).apply()
                Toast.makeText(context, "${holder.title.text} removed from favourites", Toast.LENGTH_SHORT).show()
            } else {
                newSet.add(dumpsters[position].id!!)
                editor.clear()
                editor.putStringSet(FAV_KEY, newSet).apply()
                Toast.makeText(context, "${holder.title.text} set as favourite", Toast.LENGTH_SHORT).show()
            }
           sortDumpsters(newSet)
            super.notifyDataSetChanged()
            //todo animace sorteni
        }
    }

    fun sortDumpsters(favouritesList: Set<String>) {
        val sortedList1 =  dumpsters.sortedWith(compareBy {getDistance(fragment.requireActivity() as MainActivity, it.location.getValue("latitude"),it.location.getValue("longitude"))})
        dumpsters = sortedList1.sortedWith(compareBy { compareFavourites(favouritesList.contains(it.id)) }).toMutableList()
        // todo pak jeste podle hodnoceni a abecedy
    }

    companion object {
        const val FAV_KEY = "favour"

        fun getRating(dumpster: Dumpster): String {
            val ratings: MutableList<Double> = ArrayList()
            for (comment in dumpster.comments) {
                when (comment.opinion) {
                    "GOOD" -> ratings.add(1.0)
                    "AVERAGE" -> ratings.add(3.0)
                    "SENSELESS" -> ratings.add(5.0)
                }
            }
            var rating = ratings.average()

            if (dumpster.comments.isEmpty()) {
                rating = 0.0
            }

            val d  = round(rating.times(10))
            return String.format("%.1f", d/10)
        }

        fun colorCircle(rating: String): Int {
            val unirating = rating.replace(",",".")
            val r = unirating.toDouble()
            return when {
                r == 0.0 -> {
                    Color.argb(60,241,241,241)
                }
                r < 2.33 -> {
                    Color.argb(60,127,255,127)
                }
                r < 3.66 -> {
                    Color.argb(60,255,255,63)
                }
                else -> {
                    Color.argb(60,255,35,127)
                }
            }
        }


        private fun compareFavourites(favourite: Boolean): Int{
            return if (favourite) 0 else 1
        }

        private fun getDistance (activity: MainActivity,lat: Double, lon: Double): Int {
            val myLocation = activity.mLastKnownLocation
            val dumpsterLocation = Location("")
            dumpsterLocation.longitude = lon
            dumpsterLocation.latitude = lat
            return myLocation?.distanceTo(dumpsterLocation)?.roundToInt() ?: 0
        }

        fun getDistanceString(activity: MainActivity, lat: Double, lon: Double): String{
            val distanceInMeters = getDistance(activity, lat, lon)
            return when {
                distanceInMeters == 0 -> {
                    ("distance unknown")
                }
                distanceInMeters<1000 -> {
                    ("$distanceInMeters m")
                }
                else -> {
                    var distanceInKilometers = round(distanceInMeters.toDouble()/100)
                    distanceInKilometers /= 10
                    ("$distanceInKilometers km")
                }
            }
        }
    }
}