package cz.vse.dumpstermapp.main.home.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.vse.dumpstermapp.R
import cz.vse.dumpstermapp.main.home.fragment.DumpsterDetailFragment
import cz.vse.dumpstermapp.main.home.viewholder.CommentViewHolder
import cz.vse.dumpstermapp.repository.entity.Comment
import org.koin.core.KoinComponent
import java.text.SimpleDateFormat

class ViewPagerAdapter(var comments : List<Comment>, private val fragment: DumpsterDetailFragment) : RecyclerView.Adapter<CommentViewHolder>(), KoinComponent {

    var pattern = "dd.MM. yyyy"
    var simpleDateFormat: SimpleDateFormat = SimpleDateFormat(pattern)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return CommentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {

        holder.commentRating.text = comments[position].opinion
        holder.commentText.text = comments[position].text

        if (comments[position].opinion == ""){
            Log.d("xxx","comments empty")
            holder.commentText.gravity = 17
        } else {
            holder.commentDate.text = simpleDateFormat.format(comments[position].timestamp.toDate())
        }
    }
}
