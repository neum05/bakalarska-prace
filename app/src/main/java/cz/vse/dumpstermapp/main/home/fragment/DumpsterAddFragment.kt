package cz.vse.dumpstermapp.main.home.fragment

import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.frames.abortHandler
import androidx.fragment.app.Fragment
import cz.vse.dumpstermapp.R
import cz.vse.dumpstermapp.main.MainActivity
import cz.vse.dumpstermapp.main.common.IBackAction
import cz.vse.dumpstermapp.repository.fragment.ITitleAction
import kotlinx.android.synthetic.main.fragment_dumpster_add.*

class DumpsterAddFragment : Fragment(), ITitleAction {

    val handler = Handler()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dumpster_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addDumpsterButton.setOnClickListener {
            val text = nameInputEditor.text.toString()

            if(TextUtils.isEmpty(text)) {
                nameInput.error = "Enter name"
            }else if(text.length > 30){
                nameInput.error = "Entered name is too long, max length is 30 characters"
            }else{
                (activity as MainActivity).createDumpster(text)
                addDumpsterButton.setEnabled(false)
                handler.postDelayed({
                    addDumpsterButton.text = "Check your internet connection"
                }, 2000)
            }
        }

        add_fragment_background.setOnTouchListener { v, event ->
            nameInputEditor.clearFocus()
            add_fragment_background.requestFocusFromTouch()
            (activity as MainActivity).hideKeyboard()
            add_fragment_background.performClick()
        }

        (parentFragment?.parentFragment as ITitleAction).setTitle(getString(R.string.add_dumpster_title))
    }

    override fun onDestroyView() {
        handler.removeCallbacksAndMessages(null)
        nameInput.clearFocus()
        nameInputEditor.clearFocus()
        nameInputEditor.text?.clear()
        super.onDestroyView()
    }

    override fun setTitle(title: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun unsetTitle() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun toast(s: String) {
        Log.d("xxx", "toast created")
        android.widget.Toast.makeText(context, s, android.widget.Toast.LENGTH_SHORT).show()
    }

}