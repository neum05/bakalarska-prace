package cz.vse.dumpstermapp.main.home.fragment

import android.graphics.Color
import android.opengl.Visibility
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import cz.vse.dumpstermapp.R
import cz.vse.dumpstermapp.main.MainActivity
import cz.vse.dumpstermapp.repository.fragment.ITitleAction
import kotlinx.android.synthetic.main.fragment_dumpster_add.*
import kotlinx.android.synthetic.main.fragment_dumpster_comment.*


class DumpsterCommentFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dumpster_comment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (parentFragment?.parentFragment as ITitleAction).setTitle(getString(R.string.add_comment_title))

        rating_text.setOnClickListener {
            spinnerFocus()
            rating_spinner.performClick()
        }

        rating_spinner.setOnTouchListener {v, event ->
            spinnerFocus()
            rating_spinner.performClick()
        }

        commentInputEditor.setOnTouchListener { v, view ->
            spinnerUnfocus()
            commentInputEditor.performClick()
        }

        /*commentInputEditor.setOnFocusChangeListener { v, hasFocus ->
            commentInputEditor.hint = "Accessibility, findings..."
        }*/

        comment_fragment_background.setOnTouchListener { v, event ->
            spinnerUnfocus()
            (activity as MainActivity).hideKeyboard()
            comment_fragment_background.performClick()
        }

        addCommentButton.setOnClickListener{
            val text = commentInputEditor.text.toString()
            val c = rating_spinner.selectedItem.toString()

            if(rating_text.visibility != View.GONE) {
                rating.background = ContextCompat.getDrawable(requireContext(), R.drawable.spinner_background_error)
                rating_spinner.background.setTint(ContextCompat.getColor(requireContext(), R.color.colorFormError))
                rating_hint.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorFormError))
                rating_text.setTextColor(Color.parseColor("#FFFFFF"))
                rating_hint.visibility = View.VISIBLE
                rating_error_text.visibility = View.VISIBLE
            }else{
                spinnerUnfocus()
            }
            if(TextUtils.isEmpty(text)){
                commentInput.error = "Enter comment"
            }
            if(rating_text.visibility == View.GONE && !TextUtils.isEmpty(text)) {
                (activity as MainActivity).createComment(c,text)
            }
        }
    }

    fun spinnerFocus(){
        commentInput.clearFocus()
        comment_fragment_background.requestFocusFromTouch()
        (activity as MainActivity).hideKeyboard()
        rating_text.visibility = View.GONE
        rating_hint.visibility = View.VISIBLE
        if(rating_error_text.visibility == View.GONE){
            rating.background = ContextCompat.getDrawable(requireContext(), R.drawable.spinner_background_focused)
            rating_hint.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
            rating_spinner.background.setTint(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
        }else{
            rating.background = ContextCompat.getDrawable(requireContext(), R.drawable.spinner_background_error_focused)
            rating_hint.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorFormError))
            rating_spinner.background.setTint(ContextCompat.getColor(requireContext(), R.color.colorFormError))
        }
    }

    fun spinnerUnfocus(){
        if(rating_error_text.visibility == View.GONE){
            rating.background = ContextCompat.getDrawable(requireContext(), R.drawable.spinner_background)
            rating_hint.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorTextSecondary))
            rating_spinner.background.setTint(ContextCompat.getColor(requireContext(), R.color.colorTextSecondary))
        }else{
            rating.background = ContextCompat.getDrawable(requireContext(), R.drawable.spinner_background_error)
            rating_hint.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorFormError))
            rating_spinner.background.setTint(ContextCompat.getColor(requireContext(), R.color.colorFormError))
        }
    }

}