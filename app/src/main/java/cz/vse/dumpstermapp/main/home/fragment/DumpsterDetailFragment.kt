package cz.vse.dumpstermapp.main.home.fragment

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.compose.adapters.getViewAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import cz.vse.dumpstermapp.R
import cz.vse.dumpstermapp.main.MainActivity
import cz.vse.dumpstermapp.main.common.IBackAction
import cz.vse.dumpstermapp.main.home.adapter.DumpsterAdapter
import cz.vse.dumpstermapp.main.home.adapter.ViewPagerAdapter
import cz.vse.dumpstermapp.main.home.model.HomeViewModel
import cz.vse.dumpstermapp.repository.entity.Comment
import cz.vse.dumpstermapp.repository.fragment.ITitleAction
import cz.vse.dumpstermapp.repository.shared.OnSwipeTouchListener
import cz.vse.dumpstermapp.repository.shared.Shared
import kotlinx.android.synthetic.main.fragment_dumpster_detail.*


class DumpsterDetailFragment : Fragment(), IBackAction {

    private var comments : MutableList<Comment> = ArrayList()
    private var isFavourite : Boolean = false
    private lateinit var preferences: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dumpster_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        this.requireView().setOnTouchListener(object: OnSwipeTouchListener(this.activity){
            //todo properly
            override fun onSwipeRight(){
                findNavController().navigate(R.id.action_dumpsterDetailFragment_to_dumpsterListFragment)
            }
        })

        val mViewModel = ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)
        mViewModel.selectedDumpster.observe(viewLifecycleOwner, Observer {dumpster ->

            (parentFragment?.parentFragment as ITitleAction).setTitle(dumpster.name)
            rating.text = DumpsterAdapter.getRating(dumpster)
            circle.background.setTint(DumpsterAdapter.colorCircle(rating.text.toString()))
            val distanceString =  DumpsterAdapter.getDistanceString(activity as MainActivity, dumpster.location.getValue("latitude"), dumpster.location.getValue("longitude"))
            if (distanceString == "distance unknown"){
                distance.text = distanceString
            } else {
                distance.text = distanceString + " away"
            }
            var favouritesSet = preferences.getStringSet(DumpsterAdapter.FAV_KEY,null) ?: mutableSetOf()
            if (favouritesSet.contains(dumpster.id)){
                isFavourite = true
                favourite_button.setImageResource(R.drawable.ic_heart_icon_red)
            } else {
                isFavourite = false
                favourite_button.setImageResource(R.drawable.ic_heart_icon_hollow)
            }
            setComments(dumpster.comments)
            setAdapter()

            favourite_button.setOnClickListener{
                val editor = preferences.edit()
                val newSet = HashSet<String>()
                newSet.addAll(favouritesSet)

                if (favouritesSet.contains(dumpster.id)){
                    newSet.remove(dumpster.id)
                    editor.clear()
                    editor.putStringSet(DumpsterAdapter.FAV_KEY, newSet).apply()
                    favourite_button.setImageResource(R.drawable.ic_heart_icon_hollow)
                } else {
                    newSet.add(dumpster.id!!)
                    editor.clear()
                    editor.putStringSet(DumpsterAdapter.FAV_KEY, newSet).apply()
                    favourite_button.setImageResource(R.drawable.ic_heart_icon_red)
                }
                favouritesSet = preferences.getStringSet(DumpsterAdapter.FAV_KEY,null) ?: mutableSetOf()
            }

            detail_add_comment_button.setOnClickListener{
                (activity as MainActivity).commentedDumpster = dumpster
                findNavController().navigate(R.id.dumpsterCommentFragment)
            }

            detail_navigate_to_button.setOnClickListener{
                val gmmIntentUri: Uri = Uri.parse("google.navigation:q="+dumpster.location.getValue("latitude").toString() + "," + dumpster.location.getValue("longitude").toString()+"&mode=w")
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
    }

    private fun setAdapter() {
        comment_pager.adapter = ViewPagerAdapter(comments, this)
        dots_indicator.setViewPager2(comment_pager)
    }

    private fun setComments(c: List<Comment>) {
        comments.clear()
        comments.addAll(c)
        if(c.isEmpty()){
            comments.add(Comment("This dumpster was not commented yet",""))
        }
    }

    override fun onResume() {
        super.onResume()
        Shared.dumpsterFragment = this
    }

    override fun onPause() {
        super.onPause()
        Shared.dumpsterFragment = null
    }

    override fun onBackPressed() {
        findNavController().navigate(R.id.action_dumpsterDetailFragment_to_dumpsterListFragment)

    }

}
