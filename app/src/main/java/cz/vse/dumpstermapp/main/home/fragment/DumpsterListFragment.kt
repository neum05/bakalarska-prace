package cz.vse.dumpstermapp.main.home.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.GeoPoint
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import cz.vse.dumpstermapp.R
import cz.vse.dumpstermapp.main.MainActivity
import cz.vse.dumpstermapp.main.home.adapter.DumpsterAdapter
import cz.vse.dumpstermapp.main.home.model.HomeViewModel
import cz.vse.dumpstermapp.repository.entity.Dumpster
import kotlinx.android.synthetic.main.fragment_dumpster_list.*
import androidx.lifecycle.Observer
import cz.vse.dumpstermapp.repository.fragment.ITitleAction

/**
 * A simple [Fragment] subclass.
 */
class DumpsterListFragment : Fragment() {

    private var dumpsters : MutableList<Dumpster> = ArrayList()
    private lateinit var preferences: SharedPreferences


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dumpster_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (parentFragment?.parentFragment as ITitleAction).unsetTitle()

        val mViewModel = ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)
        mViewModel.getObservable().observe(requireActivity(), Observer {
            setDumpsters(it)
            setAdapter()
        })

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
    }

    fun showDetailOf(dumpster: Dumpster){
        val mViewModel = ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)
        mViewModel.selectedDumpster.value = dumpster

        findNavController().navigate(R.id.action_dumpsterListFragment_to_dumpsterDetailFragment)
        (activity as MainActivity).centerDetail(dumpster)
    }

    fun setDumpsters(d: List<Dumpster>) {
        dumpsters.clear()
        dumpsters.addAll(d)
    }

    private fun setAdapter() {
        val d = preferences.getStringSet(DumpsterAdapter.FAV_KEY, null) ?: mutableSetOf()
        val dumpsterAdapter = DumpsterAdapter(dumpsters, preferences, this)
        dumpsterAdapter.sortDumpsters(d)


        val viewManager = LinearLayoutManager(activity)

        bottomSheetDumpsterRecycler?.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = dumpsterAdapter
        }

    }

}
