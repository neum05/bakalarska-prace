package cz.vse.dumpstermapp.main.home.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cz.vse.dumpstermapp.R
import cz.vse.dumpstermapp.main.home.model.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_tutorial.*


class HomeFragment : Fragment() {

    private var viewModel: HomeViewModel? = null
    private lateinit var prefs: SharedPreferences


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        prefs = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if(prefs!!.getBoolean("tutorial",true)){
            prefs.edit().putBoolean("tutorial",false).apply()
        }else{
            requireActivity().map_shadow.visibility = View.GONE
        }
        super.onViewCreated(view, savedInstanceState)
    }
}