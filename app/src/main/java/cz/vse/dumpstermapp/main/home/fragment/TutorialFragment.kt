package cz.vse.dumpstermapp.main.home.fragment

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.content.SharedPreferences
import androidx.fragment.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnRepeat
import cz.vse.dumpstermapp.R
import cz.vse.dumpstermapp.main.common.IBackAction
import cz.vse.dumpstermapp.main.home.model.HomeViewModel
import cz.vse.dumpstermapp.repository.shared.Shared
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_tutorial.*
import kotlinx.coroutines.delay

class TutorialFragment : Fragment() {

    private lateinit var prefs: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        prefs = requireActivity().getPreferences(Context.MODE_PRIVATE)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tutorial, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        ObjectAnimator.ofFloat(arrow_fab, "translationY", 30f).apply {
            duration = 1250
            repeatCount = Animation.INFINITE
            repeatMode = ObjectAnimator.REVERSE
            start()
        }

        ObjectAnimator.ofFloat(arrow_sheet, "translationY", -30f).apply {
            duration = 1250
            repeatCount = Animation.INFINITE
            repeatMode = ObjectAnimator.REVERSE
            start()
        }

        tutorialView.setOnClickListener{
            tutorialView.visibility = View.GONE
            requireActivity().map_shadow.visibility = View.GONE
        }

        if(prefs!!.getBoolean("tutorial",true)){
            prefs.edit().putBoolean("tutorial",false).apply()
        }else{
            tutorialView.visibility = View.GONE
        }

        super.onViewCreated(view, savedInstanceState)
    }
}