package cz.vse.dumpstermapp.main.home.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLngBounds
import com.google.firebase.firestore.GeoPoint
import cz.vse.dumpstermapp.repository.dumpsters.DumpsterFacade
import cz.vse.dumpstermapp.repository.entity.Dumpster
import cz.vse.dumpstermapp.repository.entity.Comment
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

class HomeViewModel : ViewModel(), KoinComponent {

    private val facade by inject<DumpsterFacade>()

    var selectedDumpster: MutableLiveData<Dumpster> = MutableLiveData<Dumpster>()

    fun createObservable(): LiveData<Dumpster?> {
        return facade.createObservable()
    }

    fun getObservable(): LiveData<List<Dumpster>> {
        return facade.getObservable()
    }

    fun addDumpster(dumpster: Dumpster) {
        /*var new: Dumpster? = null
        viewModelScope.launch {
            new = facade.uploadDumpster(dumpster)
        }*/

        facade.uploadDumpster(dumpster)
    }

    fun getDumpsters(bounds: LatLngBounds){
        /*viewModelScope.launch {
            facade.getDumpsters(bounds)
        }*/
        facade.getDumpsters(bounds)
    }

    fun addComment(commentedDumpster: Dumpster, comment: Comment) {
        viewModelScope.launch {
            facade.addComment(commentedDumpster, comment)
        }
    }

}