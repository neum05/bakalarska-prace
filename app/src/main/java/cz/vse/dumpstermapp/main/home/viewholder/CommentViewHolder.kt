package cz.vse.dumpstermapp.main.home.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_comment.view.*

class CommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var commentRating: TextView = view.comment_rating
    var commentText: TextView = view.comment_text
    var commentDate: TextView = view.comment_time
}