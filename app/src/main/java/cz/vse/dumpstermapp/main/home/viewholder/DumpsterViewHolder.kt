package cz.vse.dumpstermapp.main.home.viewholder

import android.graphics.drawable.shapes.Shape
import android.view.View
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_dumpster.view.*

class DumpsterViewHolder(view : View) : RecyclerView.ViewHolder(view){
    var title : TextView = view.title
    var favouriteButton: ImageButton = view.favourite_button
    var distance: TextView = view.distance
    var circle = view.circle!!
    var rating: TextView = view.rating
    var itemLayout: LinearLayout = view.itemLayout
}