package cz.vse.dumpstermapp.repository.di

import cz.vse.dumpstermapp.repository.dumpsters.DumpsterFacade
import cz.vse.dumpstermapp.repository.dumpsters.DumpsterRepository
import org.koin.dsl.module

object Module {
    val appModule = module {
        single { DumpsterFacade() }
        single { DumpsterRepository() }
    }
}