package cz.vse.dumpstermapp.repository.dumpsters

import androidx.lifecycle.LiveData
import com.google.android.gms.maps.model.LatLngBounds
import cz.vse.dumpstermapp.repository.entity.Dumpster
import cz.vse.dumpstermapp.repository.entity.Comment
import org.koin.core.KoinComponent
import org.koin.core.inject

class DumpsterFacade : IDumpsterFacade, KoinComponent{
    private val repository by inject<DumpsterRepository>()

    // umozni sahat do vice databazi, nebo rozhodovat zda pouzivat lokalni nebo online db

    fun createObservable(): LiveData<Dumpster?> {
        return repository.createObservable()
    }

    fun getObservable(): LiveData<List<Dumpster>>{
        return repository.getObservable()
    }

    override fun uploadDumpster(dumpster: Dumpster) {
        repository.uploadDumpster(dumpster)
    }

    override fun getDumpsters(bounds: LatLngBounds){
        return repository.getDumpsters(bounds)
    }

    override suspend fun addComment(commentedDumpster: Dumpster, comment: Comment) {
        return repository.addComment(commentedDumpster, comment)
    }
}