package cz.vse.dumpstermapp.repository.dumpsters

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLngBounds
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import cz.vse.dumpstermapp.main.MainActivity
import cz.vse.dumpstermapp.main.MainActivity.Companion.TAG
import cz.vse.dumpstermapp.repository.entity.Comment
import cz.vse.dumpstermapp.repository.entity.Dumpster

class DumpsterRepository : IDumpsterRepository{
    //val database = FirebaseDatabase.getInstance().getReference().database
    val db = FirebaseFirestore.getInstance()
    val docRef = db.collection("dumpsters")

    private val ld = MutableLiveData<Dumpster>()
    private val list = MutableLiveData<List<Dumpster>>()

    fun createObservable(): LiveData<Dumpster?> {
        return ld
    }

    fun getObservable(): LiveData<List<Dumpster>>{
        return list
    }

    override fun uploadDumpster(dumpster: Dumpster){

        docRef.add(dumpster).addOnSuccessListener {documentReference ->
            //set document id to dumpster.id
            Log.d("xxx", "DocumentSnapshot written with ID: ${documentReference.id}")
            dumpster.id = documentReference.id
            docRef.document(documentReference.id).update("id",documentReference.id).addOnSuccessListener {
                ld.postValue(dumpster)
            }.addOnFailureListener {
                Log.w("xxx", "Error setting dumpster id", it)
                ld.postValue(null)
            }
        }.addOnFailureListener { e ->
            Log.w("xxx", "Error adding document", e)
            ld.postValue(null)
        }
    }

    override fun getDumpsters(bounds: LatLngBounds){
        val ne = bounds.northeast
        val sw = bounds.southwest
        val dumpsters: MutableList<Dumpster> = ArrayList()

        docRef.whereLessThanOrEqualTo("location.longitude",ne.longitude)
                .whereGreaterThanOrEqualTo("location.longitude",sw.longitude)
                .get().addOnSuccessListener { documents ->
            for (document in documents) {
                val lat = document.toObject(Dumpster::class.java).location.getValue("latitude")
                //todo takhle to nikdy nemůže být! firestore sux
                if(lat <= ne.latitude && lat >=sw.latitude && document.toObject(Dumpster::class.java).enabled){
                    Log.d("Firestore document", "${document.id} => ${document.data}")
                    dumpsters.add(document.toObject(Dumpster::class.java))
                }
            }
            list.postValue(dumpsters)
        }
        .addOnFailureListener { exception ->
            list.postValue(null)
            Log.d(TAG, "get failed with ", exception)
        }
    }

    override suspend fun addComment(commentedDumpster: Dumpster, comment: Comment) {
        //todo
        commentedDumpster.comments.add(comment)
        docRef.document(commentedDumpster.id!!).set(commentedDumpster)
    }
}