package cz.vse.dumpstermapp.repository.dumpsters

import androidx.lifecycle.LiveData
import com.google.android.gms.maps.model.LatLngBounds
import cz.vse.dumpstermapp.repository.entity.Dumpster
import cz.vse.dumpstermapp.repository.entity.Comment

interface IDumpsterRepository {
    fun uploadDumpster(dumpster: Dumpster)
    fun getDumpsters(bounds: LatLngBounds)
    suspend fun addComment(commentedDumpster: Dumpster, comment: Comment) {

    }
}