package cz.vse.dumpstermapp.repository.entity

import android.annotation.SuppressLint
import com.google.firebase.Timestamp
import java.time.Clock
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId

data class Comment (
        val text: String = "AVERAGE",
        val opinion: String = "",
        val timestamp: Timestamp = Timestamp.now()
)