package cz.vse.dumpstermapp.repository.entity

import android.util.Log
import com.google.firebase.firestore.*

data class Dumpster (
    var name: String = "",
    var enabled: Boolean = true,
    var location: HashMap<String,Double> = hashMapOf("latitude" to 0.0, "longitude" to 0.0),
    var id: String? = null,
    var comments: MutableList<Comment> = ArrayList(),
    var rating: Double? = null
)