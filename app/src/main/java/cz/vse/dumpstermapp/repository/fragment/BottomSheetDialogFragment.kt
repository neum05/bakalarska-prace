package cz.vse.dumpstermapp.repository.fragment

import android.app.Dialog
import android.content.DialogInterface
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.annotation.NonNull
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.navigation.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
import com.google.android.material.bottomsheet.BottomSheetDialog
import cz.vse.dumpstermapp.R
import androidx.navigation.fragment.findNavController
import cz.vse.dumpstermapp.repository.shared.Shared
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*


open class BottomSheetDialogFragment : BottomSheetDialogFragment(), ITitleAction {

    var root: View? = null

    companion object{
        const val TAG = "BottomSheetDialogFragment"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_bottom_sheet, container, false)
        root = rootView

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (view?.parent as View).setBackgroundColor(Color.TRANSPARENT)
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            assert(view != null)
            val parent = view?.parent as View
            val layoutParams = parent.layoutParams// as FrameLayout.LayoutParams
            parent.layoutParams = layoutParams
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.apply {
            setOnShowListener {
                val dialog = it as BottomSheetDialog
                val bottomSheet = dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
                BottomSheetBehavior.from(bottomSheet!!).state = BottomSheetBehavior.STATE_EXPANDED
            }
            window?.setDimAmount(0.5f)
        }
        return bottomSheetDialog
    }

    override fun onStart() {
        super.onStart()
        dialog?.also {
            val bottomSheet = dialog?.findViewById<View>(R.id.design_bottom_sheet)
            bottomSheet?.layoutParams?.height = ViewGroup.LayoutParams.WRAP_CONTENT
            val behavior = BottomSheetBehavior.from<View>(bottomSheet!!)
            val layout = root?.findViewById(R.id.rootLayout) as CoordinatorLayout //rootLayout is root of your fragment layout.
            layout.viewTreeObserver?.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    try {
                        layout.viewTreeObserver.removeOnGlobalLayoutListener(this)
                        behavior.state = STATE_COLLAPSED
                        view?.requestLayout()
                    } catch (e: Exception) {
                    }
                }
            })
        }
    }

    override fun setTitle(title : String){
        header_title.text = title
    }

    override fun unsetTitle() {
        header_title.text = getString(R.string.bottom_sheet_header_text)
    }

    override fun onResume() {
        super.onResume()
        Shared.dumpsterTitle = this
    }

    override fun onPause() {
        super.onPause()
        Shared.dumpsterTitle = null
    }
}