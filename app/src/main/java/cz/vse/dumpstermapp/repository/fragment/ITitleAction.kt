package cz.vse.dumpstermapp.repository.fragment

interface ITitleAction {
    fun setTitle(title : String)
    fun unsetTitle()
}