package cz.vse.dumpstermapp.repository.shared

import cz.vse.dumpstermapp.main.common.IBackAction
import cz.vse.dumpstermapp.repository.fragment.ITitleAction

object Shared {

    var dumpsterFragment : IBackAction? = null

    var dumpsterTitle : ITitleAction? = null
}